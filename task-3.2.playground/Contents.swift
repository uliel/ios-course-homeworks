import UIKit

// 3.2.Пройтись по масиву використовуючи for-in, while, repeat-while і вивести всі парні величини, а потів всі непарні значення


func printArrayWithFor(arr:[Int]) {
    var even:[Int] = []
    var odd:[Int] = []
    
    for num in arr {
        if num%2 == 0 {
            even.append(num)
    }
        else {
            odd.append(num)
        }
    }
    print("Even numbers found: \(even)")
    print("Odd numbers found: \(odd)")
}
 
func printArrayWithWhile(arr:[Int]) {
    var even:[Int] = []
    var odd:[Int] = []
    var ind:Int = 0
    
    while ind < arr.count {
        if arr[ind]%2 == 0 {
            even.append(arr[ind])
        }
        else {
            odd.append(arr[ind])
        }
        ind += 1
    }
    
    print("Even numbers found: \(even)")
    print("Odd numbers found: \(odd)")
}

func printArrayWithRepeatWhile(arr:[Int]) {
    var even:[Int] = []
    var odd:[Int] = []
    var ind:Int = 0
    if arr.count != 0 {
    repeat {
        if arr[ind]%2 == 0 {
            even.append(arr[ind])
        }
        else {
            odd.append(arr[ind])
        }
        ind += 1
    }
        while ind < arr.count
    }
    
    print("Even numbers found: \(even)")
    print("Odd numbers found: \(odd)")
}


let numbers:[Int] = [1,2,3,4,5]
printArrayWithFor(arr: numbers)
printArrayWithWhile(arr: numbers)
printArrayWithRepeatWhile(arr: numbers)
