import UIKit

//Task 2(Basic data structuresmodule)

let allStudents = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

let presentOnMonday = [1, 2, 5, 6, 7]
let presentOnTuesday = [3, 6, 8, 10]
let presentOnWednesday  = [1, 3, 7, 9, 10]

// Creating a stucture to store attendance data for each student
var attendance:[(student:Int, attanded:[Int])] = []

attendance = allStudents.map({num -> (student:Int, attanded:[Int]) in
   return(num, [0,0,0]) })

// Filling in the actual attendence data
for stud in allStudents {
    
    if presentOnMonday.contains(stud) {
        attendance[stud-1].attanded[0] = 1
    }
    
    if presentOnTuesday.contains(stud) {
        attendance[stud-1].attanded[1] = 1
    }
    
    if presentOnWednesday.contains(stud) {
        attendance[stud-1].attanded[2] = 1
    }
}

// Calculating stats to filter students by attendance
var attendedThreeDays: [Int] = []
var attendedTwoDays: [Int] = []
var attendedMonWedNotTue: [Int] = []
var missedAll: [Int] = []


for stats in attendance{
    if stats.attanded.reduce(0, +) == 3 {
        attendedThreeDays.append(stats.student)
    }

    if stats.attanded.reduce(0, +) == 2 {
        attendedTwoDays.append(stats.student)
    }
    
    if stats.attanded.reduce(0, +) == 0 {
        missedAll.append(stats.student)
    }
    
    if stats.attanded[0] == 1 && stats.attanded[1] == 0 && stats.attanded[2] == 1 {
        attendedMonWedNotTue.append(stats.student)
    }
    
    
}


// Printing groups of students
print("Students that attend university three days : \(attendedThreeDays)\n")
print("Students that attend university two days : \(attendedTwoDays)\n")
print("Students that attend university on Monday and Wednesday but not Tuesday : \(attendedMonWedNotTue)\n")
print("Students that missed all classes : \(missedAll)\n")
