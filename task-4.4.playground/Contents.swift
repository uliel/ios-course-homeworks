import UIKit

// - [ ] 4.4. Функція що розвязує квадратне рівняння(в якості аргументів Double) і повертає розвязки tuple де значення будуть мати тип з попереднього завдання.

enum SolutionError:Error {
    case discriminantLessThenZero(discriminant:Double)
    case notQuadraticEq
}

public struct Complex: Equatable, CustomStringConvertible, ExpressibleByFloatLiteral{
    
    var real: Double
    var imag: Double
    
    init(real: Double, imag: Double) {
        self.real = real
        self.imag = imag
    }
}

extension Complex {
    
    public static func == (first: Complex, second: Complex) -> Bool {
        return first.real == second.real &&
            first.imag == second.imag
    }
    
    public var description: String {
        if imag != 0.0 {
        return "\(real) + \(imag)i"
        }
        else {
            return "\(real)"
        }
    }
    
    public init(floatLiteral value: Double) {
        self.init(real: value, imag: 0.0)
    }
    
    public func add(_ another: Complex) -> Complex {
        return Complex(real: self.real + another.real, imag: self.imag + another.imag)
    }
    
    
    public func subtract(_ another: Complex) -> Complex {
        return Complex(real: self.real - another.real, imag: self.imag - another.imag)
    }
    
    
    public func multiply(_ n: Double) -> Complex {
        return Complex(real: self.real * n, imag: self.imag * n)
    }
    
    public func multiply(_ another: Complex) -> Complex {
        return Complex(real: self.real * another.real, imag: self.imag * another.imag)
    }
    
    public func conjugate() -> Complex {
        return Complex(real: self.real, imag: -self.imag)
    }
    
    public func divide(_ another: Complex) -> Complex {
        return Complex(real: (self.real * another.real + self.imag*another.imag)/(pow(another.real,2) + pow(another.imag,2)), imag: (self.imag * another.real + self.real * another.imag)/(pow(another.real,2) + pow(another.imag,2)))
    }
    
    
    public func divide(_ n: Double) -> Complex {
        return Complex(real: self.real / n, imag: self.imag / n)
    }
    
    public func abs() -> Double {
        return sqrt(pow(self.real,2) + pow(self.imag,2))
    }
    
}

func solveQuadraticEq (a:Double, b:Double, c:Double) throws -> (x1:Complex, x2:Complex?){
    
    print("Solving equation : \(a)*x^2 + \(b)*x + \(c) = 0")
    
    guard a != 0 else {
        throw SolutionError.notQuadraticEq
    }
    
    let d = (pow(b, 2.0) - 4*a*c)
    print("D = \(d)")
    
    if d > 0 {
        let x1 = (-b - (d).squareRoot())/(2*a)
        let x2 = (-b + (d).squareRoot())/(2*a)
        return (Complex(floatLiteral: x1), Complex(floatLiteral: x2))
        }
        
    else if d == 0 {
        let x = (-b)/(2*a)
        return (Complex(floatLiteral: x),nil)
        }
        
    else {
        let imX1 = Complex(real: -b, imag: (-d).squareRoot()).divide(2*a)
        let imX2 = Complex(real: -b, imag: -(-d).squareRoot()).divide(2*a)
        return (imX1, imX2)
        }
}

// real solutions
print(try solveQuadraticEq(a: 1.0, b: -2.0, c: -4.0), "\n")
print(try solveQuadraticEq(a: 9.0, b: 12.0, c: 4.0), "\n")

// imaginary solutions
print(try solveQuadraticEq(a: 1, b: 4, c: 5))

