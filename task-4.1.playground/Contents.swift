import UIKit

//4.1 Функція що розвязує квадратне рівняння і повертає розвязки tuple і error enum (якщо дискримінант < 0, значення дискримінанта в помилці як Associated Value) (нам знадобиться protocol Error, throw, try);

enum SolutionError:Error {
    case discriminantLessThenZero(discriminant:Double)
    case notQuadraticEq
}



func solveQuadraticEq (a:Double, b:Double, c:Double) throws -> (x1:Double, x2:Double?){
    
    guard a != 0 else {
        throw SolutionError.notQuadraticEq
    }
    
    let d = (pow(b, 2.0) - 4*a*c)
    
    if d > 0 {
        let x1 = (-b - (d).squareRoot())/(2*a)
        let x2 = (-b + (d).squareRoot())/(2*a)
        return (x1, x2)
        }
        
    else if d == 0 {
        let x = (-b)/(2*a)
        return (x, nil)
        }
        
    else {
        throw SolutionError.discriminantLessThenZero(discriminant: d)
        }
}


try print(solveQuadraticEq(a: 1.0, b: -2.0, c: -4.0))
try print(solveQuadraticEq(a: 9.0, b: 12.0, c: 4.0))
try print(solveQuadraticEq(a: 3.0, b: 3.0, c: 2.0))
