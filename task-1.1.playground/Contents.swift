import UIKit


func meanCalculation(numbers:[Int]){
    
    let numsAmount = numbers.count
    
    if numsAmount == 0 {
        print("Cannot calculate means for 0 numbers.")
    }
    else {
    
        let summed = numbers.reduce(0, { x, y in
            x + y
        })
        let arithmeticMean = Double(summed)/Double(numsAmount)
        print("Arithmetic mean is = \(arithmeticMean)\n")
            
        
        if numbers.contains(where: { $0 < 0 }) {
            print ("Geometric mean only applies to non-negative numbers")
        }
        else {
            let multiplied = numbers.reduce(1, { x, y in
                x + y
            })
            let geometricalMean = pow(Double(multiplied), 1.0/Double(numsAmount))
            print("Geometrical mean is = \(geometricalMean)\n")
        }
    
    }
}


var nums:[Int] = [-2, 4, 6, 8, 10]
meanCalculation(numbers: nums)



