import UIKit

// 3.1.Пройтись по масиву використовуючи for-in, while, repeat-while і вивести всі величини

func printArrayWithFor(arr:[Int]) {
    for num in arr {
        print(num)
    }
}
 
func printArrayWithWhile(arr:[Int]) {
    var ind:Int = 0
    while ind < arr.count {
        print(arr[ind])
        ind += 1
    }
}

func printArrayWithRepeatWhile(arr:[Int]) {
    var ind:Int = 0
    if arr.count != 0 {
    repeat {
        print(arr[ind])
        ind += 1
    }
        while ind < arr.count
    }
}


let numbers:[Int] = [1,2,3,4,5]
printArrayWithFor(arr: numbers)
printArrayWithWhile(arr: numbers)
printArrayWithRepeatWhile(arr: numbers)
