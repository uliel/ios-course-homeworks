import UIKit

func solveQuadraticEq (a:Double, b:Double, c:Double) {
    print("Solving equation : \(a)*x^2 + \(b)*x + \(c) = 0")
    
    if a == 0{
        print("> The equation is not quadratic, please, change coeffitients.\n")
    }
    else {
        let d = (pow(b, 2.0) - 4*a*c)
        print(d)
        if d > 0 {
            let x1 = (-b - (d).squareRoot())/(2*a)
            let x2 = (-b + (d).squareRoot())/(2*a)
            print("> The roots are : x1=\(x1), x2=\(x2)\n")
        }
        else if d == 0 {
            let x = (-b)/(2*a)
            print("> The root is : x=\(x)\n")
        }
        else {
            print("> The equation has no solution.\n")
        }
    }
}


solveQuadraticEq(a: 1.0, b: -2.0, c: -4.0)
solveQuadraticEq(a: 9.0, b: 12.0, c: 4.0)
solveQuadraticEq(a: 3.0, b: 3.0, c: 2.0)
