import UIKit

// 4.3 Реалізувати оператори для роботи з комплексними числами;

public struct Complex: Equatable, CustomStringConvertible, ExpressibleByFloatLiteral{
    
    var real: Double
    var imag: Double
    
    init(real: Double, imag: Double) {
        self.real = real
        self.imag = imag
    }
}

extension Complex {
    
    public static func == (first: Complex, second: Complex) -> Bool {
        return first.real == second.real &&
            first.imag == second.imag
    }
    
    public var description: String {
        if imag != 0.0 {
        return "\(real) + \(imag)i"
        }
        else {
            return "\(real)"
        }
    }
    
    public init(floatLiteral value: Double) {
        self.init(real: value, imag: 0.0)
    }
    
    public func add(_ another: Complex) -> Complex {
        return Complex(real: self.real + another.real, imag: self.imag + another.imag)
    }
    
    
    public func subtract(_ another: Complex) -> Complex {
        return Complex(real: self.real - another.real, imag: self.imag - another.imag)
    }
    
    
    public func multiply(_ n: Double) -> Complex {
        return Complex(real: self.real * n, imag: self.imag * n)
    }
    
    public func multiply(_ another: Complex) -> Complex {
        return Complex(real: self.real * another.real, imag: self.imag * another.imag)
    }
    
    public func conjugate() -> Complex {
        return Complex(real: self.real, imag: -self.imag)
    }
    
    public func divide(_ another: Complex) -> Complex {
        return Complex(real: (self.real * another.real + self.imag*another.imag)/(pow(another.real,2) + pow(another.imag,2)), imag: (self.imag * another.real + self.real * another.imag)/(pow(another.real,2) + pow(another.imag,2)))
    }
    
    
    public func divide(_ n: Double) -> Complex {
        return Complex(real: self.real / n, imag: self.imag / n)
    }
    
    public func abs() -> Double {
        return sqrt(pow(self.real,2) + pow(self.imag,2))
    }
    
}
    
    
//var u = Complex(real: 2,imag: 1)
//var v = Complex(real: 1, imag: 3)
//var w = Complex(floatLiteral: 5.4)
//print(u.add(v))
//print(u.conjugate())
//print(u.divide(v))
//print(u.add(v).abs())
//print(w.divide(2.0))

