import UIKit

// 4.2. Реалізувати структуру для представлення комплексних чисел;

public struct Complex: Equatable, CustomStringConvertible, ExpressibleByFloatLiteral{
    
    var real: Double
    var imag: Double
    
    public init(real: Double, imag: Double) {
        self.real = real
        self.imag = imag
    }
    
    public init(floatLiteral value: Double) {
        self.init(real: value, imag: 0.0)
    }
    
    public var description: String {
        if imag != 0.0 {
        return "\(real) + \(imag)i"
        }
        else {
            return "\(real)"
        }
    }
}

    
//var u = Complex(real: 2,imag: 1)
//var v = Complex(real: 1, imag: 3)
//var w = Complex(floatLiteral: 5.4)
//print(u)
//print(v)
//print(w)
    

